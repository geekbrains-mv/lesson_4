#include<iostream>

int main()
{
    //TAKS_1
    {
        std::cout<< "TASK_1: _____________________________" <<std::endl;

        int a, b;
        std::cout<< "Input number a: " <<std::endl;
        std::cin>>a;
        std::cout<< "Input number b: " <<std::endl;
        std::cin>>b;

        if ((a + b) >= 10 && (a + b) <= 20)
            {
                std::cout<< "TRUE" <<std::endl;        
            }
        else 
        {
            std::cout<< "FALSE" <<std::endl;
        }

    }

    //TASK_2
    {
        std::cout<< "TASK_2: _____________________________" <<std::endl;

        const int x = 4, y = 6;

        if ((x == 10, y == 10) || (x + y == 10))
        {
            std::cout<< "TRUE" <<std::endl;    
        }
        else
        {
            std::cout<< "FALSE" <<std::endl;  
        }
        
    }
       
    //TASK_3
    {
        std::cout<< "TASK_3: _____________________________" <<std::endl;

        int even;

        for (even=1; even <= 50; even+=2)
        std::cout<< even <<std::endl;
    }  

    //TASK_4
    {
        std::cout<< "TASK_4: _____________________________" <<std::endl;

        int n;
        std::cout<< "Input number n: " <<std::endl;
        std::cin>>n;
        
        if (n > 0 && n % 1 == 0 && n % n  == 0)
        {
            std::cout<< "Number is sipmle" <<std::endl;
        }
        else
        {
            std::cout<< "Error! Try another one " <<std::endl;
        }
 
    }

    //TASK_5 
    {
        std::cout<< "TASK_5: _____________________________" <<std::endl;

        int y;
        std::cout<< "Input number of Year range 1-3000: " <<std::endl;
        std::cin>>y;
        
        if (y > 0 && y <= 3000)
        {
            if ( (y % 4 == 0 && y % 100 != 0) || (y % 400 == 0) )
            {
                std::cout<< "The Year is leap" <<std::endl;
            }
            else
            {
                std::cout<< "The Year is Common" <<std::endl;
            }
   
        }
        else
        {
            std::cout<< "Error!" <<std::endl;
            std::cout<< "The entered year is out of range" <<std::endl;
        }
 
    }    
    return 0;
}
 